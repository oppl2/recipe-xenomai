from conans import ConanFile, AutoToolsBuildEnvironment, tools
from conans.errors import ConanInvalidConfiguration
from conans.tools import os_info, SystemPackageTool
import contextlib
import os

required_conan_version = ">=1.33.0"


class XenomaiConan(ConanFile):
    name = "xenomai"
    license = "GPL-2.0"
    description = "linux RT paches"
    topics = ("linux kernel RT paches")
    settings = "os", "compiler", "build_type", "arch"
    generators = "make"
    _autotools = None
    _arch = "x86"
    exports_sources = ["*"]
    options = {
        "shared": [True, False],
    }
    default_options = {
        "shared": True,
    }

    @property
    def _source_subfolder(self):
        return os.path.join(self.source_folder, "source_subfolder")

    def build_requirements(self):
        self.build_requires("flex/2.6.4")
        self.build_requires("bison/3.7.6")
        self.build_requires("libtool/2.4.6")
        self.build_requires("automake/1.16.5")
        self.build_requires("pkgconf/1.7.4")

    def source(self):
        with tools.chdir(os.path.join("sources")):
            tools.untargz("xenomai-%s.tar.gz" % self.version,
                    destination=self._source_subfolder, strip_root=True)

    def validate(self):
        if self.settings.compiler == "Visual Studio":
            raise ConanInvalidConfiguration("Visual Studio not invalid")

    @contextlib.contextmanager
    def _build_context(self):
        with tools.environment_append(tools.RunEnvironment(self).vars):
            yield

    def _configure_autotools(self):
        if self._autotools:
            return self._autotools
        self._autotools = AutoToolsBuildEnvironment(self)
        conf_args = [
            "--with-core={}".format("cobalt"),
            "--enable-smp",
            "--enable-pshared",
            "--prefix=%s" % self.package_folder,
            # "--enable-shared=%s" % (self.options.shared),
            # "--enable-static=%s" % (not self.options.shared),
        ]
        self._autotools.configure(
            args=conf_args, configure_dir=self._source_subfolder)
        if self.settings.os == "Windows":
            raise ConanInvalidConfiguration("Windows builds are not supported")
        return self._autotools

    def build(self):
        with tools.chdir(self._source_subfolder):
            with self._build_context():
                self.run("scripts/bootstrap")
                autotools = self._configure_autotools()
                autotools.make()
                autotools.install()
    def deploy(self):
        self.copy("*", src="", dst="", symlinks=True)
