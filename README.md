# xenomai Linux kernel

## 简介 

为了将工作重点放在kernel调优上，统一构建环境

- 基于conan包管理系统构建

- 无需关注细节，一条命令构建

- Linux下无需安装其他软件，安装conan即可完成构建

## 基础环境

- OS：x86 Linux

- conan version > 1.33.3

## conan 环境安装

``` shell
pip3 install conan
conan config install https://gitlab.chehejia.com/liconan/conan-config.git
```

## 构建

``` shell
conan create . v3.2.1@ -pr=x64 -o ipipe=5.4.180
```

## 安装 kernel

``` shell
conan install xenomai/v3.2.1@ -pr=x64 -o ipipe=5.4.180 -r conanremote -if ./install_dir
```

ipipe=5.4.180 指的是 kernel 与 ipipe patch 的版本。这两个强关联。
5.4.180 非固定的，可随意指定内核版本号，会自动下载。

v3.2.1 指的是 xenomai 版本，可替换为 v3.2.x 等后续版本

## 目录结构

``` shell
.
|-- bin
|-- conaninfo.txt
|-- conanmanifest.txt
|-- demo
|-- etc
|-- include
|-- lib
|-- linux_kernel
|-- patches
`-- share
```

- linux_kernel 为kernel源码构建的包，可进入执行 `make install`安装到系统目录
- 其余目录均为 xenomai 依赖库 

## 文件介绍

``` txt
sources:
  v3.2.1:
    sha256: 6d04d17262746c94290c7e314c11bc5ca6a7f53a204b1509d91a2c0b4d3a36d6
    url: https://source.denx.de/Xenomai/xenomai/-/archive/v3.2.1/xenomai-v3.2.1.tar.bz2
  #kernal source
  5.4.180:
    sha256: 8c3564b44a3ecd424d79e53a0e7fd75f2af1a32a9324165cecc2109ca25d4dcf
    url: https://gitlab.chehejia.com/liyinjie/linux-kernel/-/archive/master/linux-kernel-master.tar.gz
```

- url 记录tar包下载地址，并计算sha256值，当url内容更新，请同步更新sha256
- 不同版本号会下载不同source 地址，多个source可继续累加
